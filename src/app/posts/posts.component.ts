import { Component, OnInit } from '@angular/core';
//import { Subscriber } from 'rxjs/Subscriber';
import { PostService } from '../services/post.service';
import { AppError } from '../common/app-error';
import { NotFoundError } from '../common/not-found-error';
import { BadInput } from './../common/bad-input';

@Component({
  selector: 'posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit{
  posts: any[];

  constructor(private service: PostService) {
  }

  ngOnInit() { //lifecycle hook
    this.service.getAll()
     .subscribe(posts => this.posts = posts);
  }


  createPost(input: HTMLInputElement) {
    let post = { title: input.value };
    input.value = '';

    this.service.create(post)
      .subscribe(
        newPost => {
          post['id'] = newPost.id;
            this.posts.splice(0, 0, post);
          },
          (error: AppError) => {
            if (error instanceof BadInput) {
              // this.form.setErrors(error.originalError);
            }
            else throw error;
          });
  }

  updatePost(post) {
    this.service.update(post)
      .subscribe(
        updatedPost => {
          console.log(updatedPost);
        });
  }

  deletePost(post) {
    this.service.delete(post.id)
      .subscribe(
        () => {
          let index = this.posts.indexOf(post);
          this.posts.splice(index, 1);
        },
        (error: AppError) => {
          if (error instanceof NotFoundError)
            alert('This post has already been deleted.');
          else throw error;
        });
  }
    /*
       Manteniendo un servidor para el post particular 
    .subscribe(response => {
      //console.log(response.json());
      this.posts = response.json();
    }, 
    error => {
      alert('An unexpected error occurred.');
      console.log(error);
    });
  }*/
/*
  createPost(input: HTMLInputElement){
    let post = { title: input.value };
    input.value = '';

    this.service.createPost(post)
      .subscribe(response => {
        post['id'] = response.json().id;
        this.posts.splice(0, 0, post);
      }, 
      error => {
        alert('An unexpected error occurred.');
        console.log(error);
      });
  }

  updatePost(post){
    //this.http.patch(this.url + '/' + post.id, JSON.stringify({ isRead: true }))
    this.service.updatePost(post.id)
      .subscribe(response => {
        console.log(response.json());
      }, 
      error => {
        alert('An unexpected error occurred.');
        console.log(error);
      });
  }

  deletePost(post){
    //this.http.delete(this.url + '/' + post.id)
    this.service.deletePost(222222222222)
      .subscribe(response => {
        let index = this.posts.indexOf(post);
        this.posts.splice(index, 1);
      }, 
      (error: AppError ) => {
        console.log(error);
        if (error instanceof NotFoundError)
          alert('This post has already been deleted.');
        else {
          alert('An unexpected error occurred.');
          console.log(error);
        }
      });
  }*/
}
