/*
Se comenta este codigo version anterior

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';  //esta libreria permite el manejo de capturas de errores 
import { AppError } from '../common/app-error';
import { NotFoundError } from '../common/not-found-error';
import { BadInput } from '../common/bad-input';

@Injectable()
export class PostService {
  private url = 'https://jsonplaceholder.typicode.com/posts'; //esta página permite simular una BD y retorna datos desde un servidor

  constructor(private http: Http) { }

  getpost() {
    return this.http.get(this.url)  // este método retorna un observable
      .catch(this.handleError);
  }

  createPost(post) {
    return this.http.post(this.url, JSON.stringify(post))
      .catch(this.handleError);
  }

  updatePost(id: number) {
    return this.http.patch(this.url + '/' + id, JSON.stringify({ isRead: true }))
      .catch(this.handleError);
  }

  deletePost (id: number) {
    return this.http.delete(this.url + '/' + id)
      .catch(this.handleError);
      /*.catch((error: Response) => { // se obtiene el error que es una instancia del objeto response 
        if (error.status === 404)
          return Observable.throw(new NotFoundError());

        return Observable.throw(new AppError(error));  // como catch retorna un obsevable 
      });*/
 /* }

  private handleError(error: Response) {
    if (error.status === 400)
      return Observable.throw(new BadInput(error.json()));

    if (error.status === 404)
      return Observable.throw(new NotFoundError());

    return Observable.throw(new AppError(error));  // como catch retorna un obsevable 
  }
}*/

import { DataService } from './data.service';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class PostService extends DataService {
  constructor(http: Http) {
    super('http://jsonplaceholder.typicode.com/posts', http);
   }
}
